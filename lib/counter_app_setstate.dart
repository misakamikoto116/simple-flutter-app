import 'package:d_method/d_method.dart';
import 'package:flutter/material.dart';

// penjelasan :
// - Bedanya sama yang counter_app (getX), disini sama saja ngerebuild ulang
// dengan trigger setState, maka dari itu sebenarnya jika componennya banyak
// pake seperti ini jadi lebih berat.. bisa sih pake ini tapi effort bnget
// harus bikin satu satu State<blabla> nya per 1 state
// - initState hanya dipanggil sekali saja saat buka dan dispose saat tutup
class CounterAppSetState extends StatefulWidget {
  const CounterAppSetState({super.key});

  @override
  State<CounterAppSetState> createState() => _CounterAppSetStateState();
}

class _CounterAppSetStateState extends State<CounterAppSetState> {
  @override
  void initState() {
    DMethod.printBasic('initState() - CounterAppSetState');
    super.initState();
  }

  @override
  void dispose() {
    DMethod.printBasic('dispose() - CounterAppSetState');
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    DMethod.printBasic('build() - CounterAppSetState');
    int counter = 1;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Counter App SetState'),
        centerTitle: true,
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton.filled(
                onPressed: () {
                  counter++;
                  setState(() {});
                },
                icon: const Icon(Icons.remove)),
            const SizedBox(width: 20),
            Text(
              '$counter',
              style: Theme.of(context).textTheme.displaySmall,
            ),
            const SizedBox(width: 20),
            IconButton.filled(
                onPressed: () {
                  counter--;
                  setState(() {});
                },
                icon: const Icon(Icons.add))
          ],
        ),
      ),
    );
  }
}
