import 'package:get/get.dart';

class SimpleCrudController extends GetxController {
  final _list = <String>[].obs;
  List<String> get list => _list;

  add(String n) {
    _list.add(n);
    update();
  }

  updateItem(int index, String n) {
    _list[index] = n;
    update();
  }

  delete(int index) {
    _list.removeAt(index);
    update();
  }

  clearState() {
    Get.delete<SimpleCrudController>(force: true);
  }
}
