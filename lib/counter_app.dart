import 'package:d_method/d_method.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// Cara Wrap auto bisa pake ctrl+.
// DMethod hanya sekali saja dipanggil, ya.. memang begitulah stateless Widget
// nah kenapa bisa berubah ubah counternya? karena semua yang di Obx dan
// final obs, itu masuknya ke stateful.. jadi yang didalam obx itu diterkecuali
// kan
class CounterApp extends StatelessWidget {
  const CounterApp({super.key});

  @override
  Widget build(BuildContext context) {
    DMethod.printBasic('build() - CounterApp');
    final counter = 1.obs;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Counter App'),
        centerTitle: true,
      ),
      body: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            IconButton.filled(
                onPressed: () {
                  counter.value--;
                },
                icon: const Icon(Icons.remove)),
            const SizedBox(width: 20),
            Obx(() {
              int number = counter.value;
              return Text(
                '$number',
                style: Theme.of(context).textTheme.displaySmall,
              );
            }),
            const SizedBox(width: 20),
            IconButton.filled(
                onPressed: () {
                  counter.value++;
                },
                icon: const Icon(Icons.add))
          ],
        ),
      ),
    );
  }
}
